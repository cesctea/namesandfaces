//added new action type: LOAD_PERSON_ID_SUCCESS
export default function peopleReducer(state = {}, action) {
  switch (action.type) {
    case "LOAD_PEOPLE_SUCCESS":
      console.log("REDUCER: LOAD_PEOPLE_SUCCESS");
      return {
        ...state,
        peoplePayload: action.payload
      };
    case "SET_PERSONID_SUCCESS":
      console.log("REDUCER: SET_PERSONID_SUCCESS");
      return {
        ...state,
        selectedPersonId: action.payload
      };
    default:
      return state;
  }
}
