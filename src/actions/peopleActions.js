import FakeApi from "../api/fakeApi";

const peopleActions = {};

function loadPeopleSuccess(people) {
  return { type: "LOAD_PEOPLE_SUCCESS", payload: people };
}

function setSelectedPersonId(personId) {
  return {
    type: "SET_PERSONID_SUCCESS",
    payload: personId
  };
}

//passed in personId
//Added second dispatch (setSelectedPersonId)
//updated catch
peopleActions.loadPeople = personId => {
  return async function(dispatch) {
    return FakeApi.loadPeople()
      .then(people => {
        dispatch(loadPeopleSuccess(people));
        if (personId) {
          dispatch(setSelectedPersonId(personId));
        }
      })
      .catch(error => {
        console.log("peopleActions.loadPeople error", error);
      });
  };
};

export default peopleActions;
