import delay from "./delay";
import RawData from '../mockedData.json'

class FakeApi {
  static loadPeople() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], RawData));
      }, delay);
    });
  }
}

export default FakeApi;
