import React from "react";
import Card from "react-bootstrap/Card";
import _ from "lodash";

class SelectedPersonCard extends React.Component {
  constructor(props) {
    super(props);
    this.updateSelectedPerson = this.updateSelectedPerson.bind(this);
  }

  updateSelectedPerson = () => {
    const thisComponent = this;
    thisComponent.props.updateFunction(thisComponent.props.peopleData.personId);
  };

  //null check before render
  //added new styling class
  render() {
    const thisComponentProps = this.props;
    if (
      _.isEmpty(thisComponentProps.peopleData) ||
      _.isEmpty(thisComponentProps.peopleData.photoUrl)
    ) {
      return <div />;
    }
    return (
      <div className="Selected">
        <Card>
          <Card.Img
            variant="top"
            src={require("../photos/" + thisComponentProps.peopleData.photoUrl)}
          />
          <Card.Body>
            <Card.Title>
              {thisComponentProps.peopleData.firstName}{" "}
              {thisComponentProps.peopleData.lastName}
              <br />
              {thisComponentProps.peopleData.role}
            </Card.Title>
            <Card.Text />
          </Card.Body>
          <Card.Footer />
        </Card>
      </div>
    );
  }
}

export default SelectedPersonCard;
