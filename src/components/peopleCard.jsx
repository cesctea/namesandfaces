import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

class PeopleCard extends React.Component {
  render() {
    const thisComponentProps = this.props;

    return (
      <Card>
        <Card.Img
          variant="top"
          src={require("../photos/" + thisComponentProps.peopleData.photoUrl)}
        />
        <Card.Body>
          <Card.Title>
            {thisComponentProps.peopleData.firstName}{" "}
            {thisComponentProps.peopleData.lastName}
            <br />
            {thisComponentProps.peopleData.role}
          </Card.Title>
          <Card.Text />
        </Card.Body>
        <Card.Footer>
          <Button
            onClick={() =>
              thisComponentProps.selectedPerson(
                thisComponentProps.peopleData.personId
              )
            }
          >
            View Card
          </Button>
        </Card.Footer>
      </Card>
    );
  }
}

export default PeopleCard;
