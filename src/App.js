import React, { Component } from "react";
import { connect } from "react-redux";
//new imports
import PeopleCard from "./components/peopleCard";
import PeopleActions from "./actions/peopleActions";
import SelectedPersonCard from "./components/selectedPersonCard";
import _ from "lodash";
//layout
import "./App.css";
import logo from "./logo.svg";

class App extends Component {
  //new compomentDidMount() to select person id from route
  //route prop check
  //default load all people (id = null)
  componentDidMount() {
    if (
      this.props.match &&
      this.props.match.params &&
      this.props.match.params.personId
    ) {
      this.props.loadPeopleAndSelectPersonFromRouteAsync(
        this.props.match.params.personId
      );
    } else {
      this.props.loadPeopleAsync();
    }
  }

  state = {
    selectedPerson: {}
  };

  //better naming, viewCard => selectedPersonUpdate
  //imported SelectedPersonCard
  render() {
    const thisComponentProps = this.props;
    return (
      <div className="App">
        <img src={logo} className="fit" alt="logo" />
        <br />
        <p />
        <div className="Cards">
          {_.map(thisComponentProps.people, peopleData => (
            <PeopleCard
              key={peopleData.personId}
              peopleData={peopleData}
              selectedPerson={this.updateSelectedPerson.bind(this)}
            />
          ))}
          
        </div>
        <SelectedPersonCard peopleData={this.getPerson()} />
      </div>
    );
  }

  //new method
  getPerson = () => {
    if (!this.state.selectedPerson.personId) {
      return _.find(this.props.people, [
        "personId",
        this.props.selectedPersonId
      ]);
    } else {
      return this.state.selectedPerson;
    }
  };

  //new set state logic
  updateSelectedPerson = personId => {
    if (!personId) return;
    let selectedPerson = _.find(this.props.people, ["personId", personId]);
    this.setState({
      selectedPerson
    });
  };
}

//new prop
const mapStateToProps = state => ({
  ...state,
  people: state.peoplePayload === undefined ? [] : state.peoplePayload,
  selectedPerson: state.selectedPersonId === undefined ? "" : state.selectedPersonId
});

const mapDispatchToProps = dispatch => ({
  loadPeopleAsync: () => dispatch(PeopleActions.loadPeople()),
  loadPeopleAndSelectPersonFromRouteAsync: personId =>
    dispatch(PeopleActions.loadPeople(personId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
