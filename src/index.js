import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import configureStore from './store';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//imported router & single page route paths
ReactDOM.render(
  <Router>
    <Provider store={configureStore()}>
      <Route exact path="/" component={App} />
      <Route path="/:personId" component={App} />
    </Provider>
  </Router>,
  document.getElementById('root')
);

serviceWorker.unregister();
