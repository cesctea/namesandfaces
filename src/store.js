import { createStore, applyMiddleware } from "redux";
import reducers from './reducers/mainReducer'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import logger from 'redux-logger'

export default function configureStore(initialState) {
	return createStore(    
    	reducers, 
		initialState,
		applyMiddleware(thunk, reduxImmutableStateInvariant(), logger)
	);
}