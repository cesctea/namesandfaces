

## To run 


```npm install```

 ```npm start```



**wanted to start with barebone project but with a dev build & webpack config setup.. <br>
Then moved onto:**

- added store, actions and reducer files
- imported Provider and configured app & store
-  created fake JSON data to import 
- setup FakeApi and added loadPeople method
- imported thunk & redux-immuteable to dispatch loadPeople
- fleshed out reducer to handle action type
- connected props to app redux state
- added components - used react-bootstrap for the cards 
- bound state data to component prop attributes
- add basic styling to the components
- added selectedPersonCard and functions
- rendered selectedPersonCard based on button click id or route
- 

next steps: 
* add nav bar & react routing
* host site on Heroku/S3 single page app
* docker file create
* unit tests


packages installed:
* "bootstrap": "^4.3.1",
* "lodash": "^4.17.11",
* "react": "^16.8.6",
* "react-bootstrap": "^1.0.0-beta.8",
* "react-dom": "^16.8.6",
* "react-redux": "^7.0.3",
* "react-scripts": "3.0.1",
* "reactstrap": "^8.0.0",
* "redux": "^4.0.1",
* "redux-immutable-state-invariant": "^2.1.0",
* "redux-logger": "^3.0.6",
* "redux-thunk": "^2.3.0"